import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home";
import OrderList from "./views/OrderList";
import ProductDetails from "./views/ProductDetails";


Vue.use(VueRouter)

const routes = [
    {path: '/', component: Home},
    {path: '/product-details/:id', component: ProductDetails, name : 'product'},
    {path: '/order-list', component: OrderList},

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.APP_URL,
    routes,
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});


export default router;
